const mongoose = require("mongoose");
const {DrinkModel} = require("../model/DrinkModel");

function createDrink (req, res) {
    const drink = new DrinkModel({
        _id: mongoose.Types.ObjectId(),
        maNuocUong: req.body.maNuocUong,
        tenNuocUong: req.body.tenNuocUong,
        donGia: req.body.donGia,
        ghiChu: req.body.ghiChu,
        ngayTao: req.body.ngayTao,
        ngayCapNhat: req.body.ngayCapNhat
    });
    drink.save()
        .then((newDrink)=>{
            return res.status(200).json({
                message:"Success",
                drink: newDrink
            })
        })
        .catch((error)=>{
            return res.status(500).json({
                message: "Fail",
                error: error.message
            })
        })
}
function getAllDrink (request, response) {
    DrinkModel.find()
        .select("_id maNuocUong tenNuocUong donGia ghiChu ngayTao ngayCapNhat")
        .then((drinkList) => {
            return response.status(200).json({
                message: "Success",
                drink: drinkList
            })
        })
        .catch((error) => {
            return response.status(500).json({
                message: "Fail",
                error: error.message
            })
        })
}

module.exports = {createDrink, getAllDrink};