const mongoose = require("mongoose");
const {VoucherModel} = require("../model/VoucherModel");

function createVoucher (req, res) {
    const voucher = new VoucherModel({
        _id: mongoose.Types.ObjectId(),
        maVoucher: req.body.maVoucher,
        phanTramGiamGia: req.body.phanTramGiamGia,
        ghiChu: req.body.ghiChu,
        ngayTao: req.body.ngayTao,
        ngayCapNhat: req.body.ngayCapNhat
    });
    voucher.save()
        .then((newVoucher)=>{
            return res.status(200).json({
                message:"Success",
                voucher: newVoucher
            })
        })
        .catch((error)=>{
            return res.status(500).json({
                message: "Fail",
                error: error.message
            })
        })
}
function getAllVoucher (request, response) {
    VoucherModel.find()
        .select("_id maVoucher phanTramGiamGia ghiChu ngayTao ngayCapNhat")
        .then((voucherList) => {
            return response.status(200).json({
                message: "Success",
                vouchers: voucherList
            })
        })
        .catch((error) => {
            return response.status(500).json({
                message: "Fail",
                error: error.message
            })
        })
}

module.exports = {createVoucher, getAllVoucher};