//Import thư viện mongo
const mongoose = require("mongoose");
//Sử dụng phép gán phá hủy cấu trúc để lấy thuộc tính Scheme
const {Schema} = mongoose;
//Khởi tạo voucher Scheme MongoDB
const drinkScheme = new Schema(
    {
        _id: Schema.Types.ObjectId,
        maNuocUong: {
            type: String,
            require: true,
            unique: true
        },
        tenNuocUong: {
            type: String,
            required: true,
            unique: true
        },
        donGia: {
            type: Number,
            required: true,
        },
        ghiChu: {
            type: String,
            required: false,
            default: null
        },
        ngayTao: {
            type: Date,
            required: false,
            default: Date.now()
        },
        ngayCapNhat: {
            type: Date,
            required: false,
            default: Date.now()
        }
    }
);
//Tạo drink Model
const DrinkModel = mongoose.model("Drinks", drinkScheme);
//Export Drink Model
module.exports = {DrinkModel};