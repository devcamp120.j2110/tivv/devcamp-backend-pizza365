//Import thư viện mongo
const mongoose = require("mongoose");
//Sử dụng phép gán phá hủy cấu trúc để lấy thuộc tính Scheme
const {Schema} = mongoose;
//Khởi tạo voucher Scheme MongoDB
const voucherScheme = new Schema(
    {
        _id: Schema.Types.ObjectId,
        maVoucher: {
            type: Number,
            require: true,
            unique: true
        },
        phanTramGiamGia: {
            type: Number,
            required: true,
            unique: false
        },
        ghiChu: {
            type: String,
            required: false,
            default: null
        },
        ngayTao: {
            type: Date,
            required: false,
            default: Date.now()
        },
        ngayCapNhat: {
            type: Date,
            required: false,
            default: Date.now()
        }
    }
);
//Tạo voucher Model
const VoucherModel = mongoose.model("Voucher", voucherScheme);
//Export Course Model
module.exports = {VoucherModel};