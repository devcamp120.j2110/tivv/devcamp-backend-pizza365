const express = require("express");
const router = express.Router();
const { createVoucher, getAllVoucher  } = require("../controller/VoucherController");
router.post("/", createVoucher);
router.get("/", getAllVoucher);

module.exports = router;