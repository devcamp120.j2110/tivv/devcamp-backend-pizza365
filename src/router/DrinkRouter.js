const express = require("express");
const router = express.Router();
const { createDrink, getAllDrink  } = require("../controller/DrinkController");
router.post("/", createDrink);
router.get("/", getAllDrink);

module.exports = router;