
const express = require("express");
const mongoose = require("mongoose");

const VoucherRouter = require("./src/router/VoucherRouter");
const DrinkRouter = require("./src/router/DrinkRouter");

const port = 3000;
const app = express();
//Khai bao lay tieng viet
app.use(express.urlencoded({
    extended:true
}));
//khai bao body dang JSON
app.use(express.json());
//Connect to mongoDB
async function ConnectMongoDB() {
    await mongoose.connect("mongodb://localhost:27017/CRUD_VOUCHER");
};
//Excute connect
ConnectMongoDB()
    .then(()=>console.log("Connect to MongoDB successfully!"))
    .catch(()=>console.log("Connect fail"));

app.get("/", (req, res)=>{
    res.json({
        message: "CRUD API"
    })
});

app.use("/vouchers",VoucherRouter);
app.use("/drinks", DrinkRouter);

app.listen(port, ()=>{
    console.log(`App listening on port ${port}`);
});